function (newDoc, oldDoc, userCtx) {
    var doc_type = newDoc['type'];

    function forbidden(message) {
        throw({forbidden : message});
    }

    function require(beTrue, message) {
        if (!beTrue)
            forbidden(message);
    }

    function is_admin(roles) {
        return (roles.indexOf("_admin") != -1 || roles.indexOf("mlf2_admin") != -1);
    }

    if(newDoc._deleted) {
        require(is_admin(userCtx.roles),
                "Only admin users can delete a document");
        return;
    }

    require(doc_type == "local" || doc_type === "proxy" || doc_type == "session",
	    "Invalid document type");
    require(newDoc.imei, "Iridium IMEI must be specified");

    if(doc_type == "local") {
        require(is_admin(userCtx.roles),
            "Only admin users can add/change this document type");
    }

    if(doc_type == "proxy") {
        require(is_admin(userCtx.roles),
                "Only admin users can add/change this document type");
        require(newDoc.owner, "Owner must be specified");
        require(newDoc.endpoint && newDoc.endpoint.indexOf(':') != -1,
            "You must specify a TCP endpoint, <host>:<port>");
    }

}
