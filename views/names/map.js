function(doc) {
    if(doc.type == "proxy" || doc.type == "local") {
	emit(doc.name, doc);
    }
}
