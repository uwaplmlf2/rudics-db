function() {
    var form = $(this);
    var fdoc = form.serializeObject();

    // Sanity checks
    $("#statusList").empty().removeClass();
    var errors = [];
    if (!fdoc.endpoint) {
        var re = /^mlf2-\d+/;
        if(fdoc.name.match(re)) {
            fdoc.type = "local";
        } else {
            errors.push("Invalid name for MLF2 device: " + fdoc.name);
        }
    } else {
        var parts = fdoc.endpoint.split(":");
        var port = parseInt(parts[1]);
        if (port < 1 || port > 65535)
            errors.push("Invalid TCP port");
        fdoc.type = "proxy";
        if(!fdoc.owner) {
            errors.push("You must specify an owner email address");
        }
    }

    if (!fdoc.imei) {
        errors.push("You must specify an IRSN");
    }

    if(errors.length > 0) {
        $("#statusList").addClass("error");
        $.each(errors, function(index, e) {
            $("#statusList").append("<li>" + e + "</li>");
        });
        return false;
    }

    fdoc._id = fdoc.imei;
    $$(this).app.db.saveDoc(fdoc, {
				success : function() {
				    form[0].reset();
				}
			    });
    return false;
}
