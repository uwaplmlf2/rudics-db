function(data) {
    var items = [];
    var item = {};
    var count = 0;

    $.each(data.rows, function(index, row){
        if(row.key[1] == 0) {
            item.imei = row.doc.imei;
            item.t_connect = (new Date(row.doc.connect*1000)).toUTCString();
            item.dt_connect = row.doc.login - row.doc.connect;
            item.dt_login = row.doc.logout - row.doc.login;
            item.bytes_in = row.doc.bytes_in;
            item.bytes_out = row.doc.bytes_out;
            count += 1;
        } else if(row.key[1] == 1 && row.doc) {
            item.name = row.doc.name;
            count += 1;
        }

        if(count == 2){
            items.push(item);
            item = {};
            count = 0;
        }
    });

    return {
        items : items
    };
}
